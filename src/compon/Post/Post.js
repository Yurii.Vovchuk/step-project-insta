import React from 'react';

const Post = (props) => {
  const {src, alt} = props

  return(
    <div >
      <img src={src} alt={alt} className='post__image'></img>
    </div>
  )
}

export default Post;
