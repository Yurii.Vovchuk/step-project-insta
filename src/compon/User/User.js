import React from 'react';
import './User.scss'

const User = (props) => {

const {src, alt, name} = props;

  return(
    <a href='#' className='user__link'>
      <div className='avatar__wrap'>
        <div className='avatar__big-circle'>
          <img src={src} alt={alt} className='user__avatar'></img>
        </div>
        <div user__name>{name}</div>
      </div>
    </a>
  )
}

export default User;
